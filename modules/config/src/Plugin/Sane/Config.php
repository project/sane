<?php

namespace Drupal\sane_config\Plugin\Sane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadInterface;
use Drupal\sane\PluginBase;
use Drupal\sane_config\Payload;

/**
 * Plugin implementation of the sane.
 *
 * @Sane(
 *   id = "config",
 *   label = @Translation("Config"),
 *   description = @Translation("Manages all config entities.")
 * )
 */
class Config extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function assertPayload(PayloadInterface $payload): bool {
    return $payload instanceof Payload;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedSubscriptions($roles): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(&$form, FormStateInterface $form_state) {
    // TODO: Implement buildForm() method.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl(EventInterface $event): Url {
    return Url::fromRoute('<front>');
  }

}
