<?php

namespace Drupal\sane_config;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Drupal\sane\PluginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Config subscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\sane_config\Plugin\Sane\Config
   */
  protected $plugin;

  /**
   * @var bool
   */
  protected $active;

  /**
   * Constructs a new Settings object.
   *
   * @param \Drupal\sane\PluginManager $plugin_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(PluginManager $plugin_manager) {
    $this->plugin = $plugin_manager->createInstance('config');
    $this->active = TRUE;
  }

  /**
   * @return bool
   *   Protected function enabled.
   */
  protected function enabled(): bool {
    // TODO: Make this configurable.
    // ATTENTION: while installing the module, keep this disabled!!!
    return FALSE;
  }

  /**
   * Saves changed config to a configurable directory.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Public function onConfigSave event.
   */
  public function onConfigSave(ConfigCrudEvent $event): void {
    if ($this->active && $this->enabled()) {
      // TODO: Move this to the plugin.
      #$object = $event->getConfig();
      #$this->plugin->createEvent('config', 'Config changed', new ConfigPayload($object));
    }
  }

  /**
   * Saves changed config translation to a configurable directory.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   Public function onConfigTranslationSave event.
   */
  public function onConfigTranslationSave(LanguageConfigOverrideCrudEvent $event): void {
    if ($this->active && $this->enabled()) {
      // TODO: Move this to the plugin.
      /** @var \Drupal\language\Config\LanguageConfigOverride $object */
      $object = $event->getLanguageConfigOverride();
      #$this->plugin->createEvent('language', 'Config translation changed', new ConfigPayload($object));

    }
  }

  /**
   * Turn off this subscriber on importing configuration.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   Public function onConfigImportValidate event.
   */
  public function onConfigImportValidate(ConfigImporterEvent $event): void {
    $this->active = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onConfigSave', 0];
    $events[ConfigEvents::IMPORT_VALIDATE][] = ['onConfigImportValidate', 1024];
    if (class_exists(LanguageConfigOverrideEvents::class)) {
      $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE][] = ['onConfigTranslationSave', 0];
    }
    return $events;
  }

}
