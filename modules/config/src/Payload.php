<?php

namespace Drupal\sane_config;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\node\Entity\Node;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadBase;
use Drupal\sane\PayloadInterface;

class Payload extends PayloadBase {

  protected $object;

  /**
   * Content constructor.
   *
   * @param $object
   */
  public function __construct($object) {
    $this->object = $object;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventReference(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriptionReference(EventInterface $event): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function prepareArray(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromArray(array $payload): PayloadInterface {
    return new static(NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): ContentEntityInterface {
    /** @var \Drupal\node\NodeInterface $entity */
    $entity = Node::create([
      'title' => 'not yet implemented',
    ]);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccess($uid): bool {
    return FALSE;
  }

}
