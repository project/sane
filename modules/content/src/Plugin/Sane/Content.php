<?php

namespace Drupal\sane_content\Plugin\Sane;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadInterface;
use Drupal\sane\PluginBase;
use Drupal\sane_content\Payload;

/**
 * Plugin implementation of the sane.
 *
 * @Sane(
 *   id = "content",
 *   label = @Translation("Content"),
 *   description = @Translation("Manages all content entity types and their
 *   bundles.")
 * )
 */
class Content extends PluginBase {

  use ContentSettingsTrait;

  public const SETTING_CREATE_EVENT = 'create_event';

  public const TOPIC_CREATE = 'create';

  public const TOPIC_UPDATE = 'update';

  public const TOPIC_DELETE = 'delete';

  public const TOPIC_PUBLISH = 'publish';

  public const TOPIC_UNPUBLISH = 'unpublish';

  /**
   * {@inheritdoc}
   */
  public function assertPayload(PayloadInterface $payload): bool {
    return $payload instanceof Payload;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedSubscriptions($roles): array {
    $subscriptions = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $bundles = array_keys($this->bundleInfo->getBundleInfo($definition->id()));
        if (empty($bundles)) {
          $bundles = [$definition->id()];
        }
        foreach ($bundles as $bundle) {
          $config_id = $this->configId($definition->id(), $bundle);
          $config = $this->configFactory->get($config_id);
          foreach ($this->topics() as $topic => $topicDef) {
            $relevantRoles = array_intersect(
              $config->get($this->getSettingKey('allow_subscription', $topic)) ?? [],
              $roles
            );
            if (!empty($relevantRoles)) {
              $subscriptions[implode('-', [
                $this->getPluginId(),
                $definition->id(),
                $bundle,
                $topic,
              ])] = $this->t('Subscribe to content @type - @topic', [
                '@type' => implode('-', [$definition->id(), $bundle]),
                '@topic' => $topic,
              ]);
            }
          }
        }
      }
    }
    return $subscriptions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(&$form, FormStateInterface $form_state) {
    $info = $form_state->getBuildInfo();
    if (!empty($info['callback_object']) && $info['callback_object'] instanceof ContentEntityFormInterface) {
      /** @var ContentEntityFormInterface $callback */
      $callback = $info['callback_object'];
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $callback->getEntity();
      if ($callback instanceof ContentEntityDeleteForm) {
        $topics = [self::TOPIC_DELETE, self::TOPIC_UNPUBLISH];
      }
      else {
        $topics = $entity->isNew() ? [self::TOPIC_CREATE] : [self::TOPIC_UPDATE];
        if ($this->isPublished($entity)) {
          $topics[] = self::TOPIC_UNPUBLISH;
        }
        else {
          $topics[] = self::TOPIC_PUBLISH;
        }
      }
      if ($this->getSetting(self::SETTING_CREATE_EVENT, $topics, $entity->getEntityTypeId(), $entity->bundle())) {
        return $this->buildContentForm($form, $form_state, $entity, $topics[0]);
      }
    }
    else {
      $route_name = $this->routeMatch->getRouteName();
      foreach ($this->entityTypeManager->getDefinitions() as $definition) {
        if (($definition instanceof ContentEntityTypeInterface) && ($field_ui_base_route = $definition->get('field_ui_base_route')) && $field_ui_base_route === $route_name) {
          return $this->buildContentTypeSettingsForm($form, $form_state, $definition);
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl(EventInterface $event): Url {
    /** @var \Drupal\sane_content\Payload $payload */
    $payload = $event->getPayload();
    try {
      return $payload->getEntity()->toUrl();
    } catch (EntityMalformedException $e) {
      return Url::fromRoute('<front>');
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return bool
   */
  protected function isPublished(ContentEntityInterface $entity): bool {
    if (!$entity->getEntityType()->hasKey('status')) {
      return TRUE;
    }
    return (bool) $entity->get($entity->getEntityType()
      ->getKey('status'))->value;
  }

  /**
   * @param string $key
   * @param array|string $topicKeys
   * @param string $entityType
   * @param string $bundle
   *
   * @return bool
   */
  protected function getSetting($key, $topicKeys, $entityType, $bundle = NULL): bool {
    if (is_string($topicKeys)) {
      $topicKeys = [$topicKeys];
    }
    $config = $this->configFactory->get($this->configId($entityType, $bundle));
    foreach ($topicKeys as $topic) {
      if ($config->get($this->getSettingKey($key, $topic))) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $definition
   *
   * @return array
   */
  protected function buildContentTypeSettingsForm(array &$form, FormStateInterface $form_state, ContentEntityTypeInterface $definition): array {
    if (($bundleKey = $definition->getBundleEntityType()) && $parameter = $this->routeMatch->getParameter($bundleKey)) {
      $bundle = $parameter->id();
    }
    else {
      $bundle = NULL;
    }
    $config_id = $this->configId($definition->id(), $bundle);
    $config = $this->configFactory->get($config_id);
    $form_state->setTemporaryValue(['sane', 'config_id'], $config_id);
    $roles = $this->query->rolesAsSelectList();

    $header = [''];
    foreach ($this->topics() as $topicDef) {
      $header[] = $topicDef['label'];
    }

    $form['sane'] = [
      '#type' => 'details',
      '#group' => isset($form['additional_settings']) ? 'additional_settings' : 'advanced',
      '#title' => $this->t('Sane'),
      '#weight' => 11,
    ];
    $form['sane']['events'] = [
      '#type' => 'table',
      '#sticky' => TRUE,
      '#tree' => TRUE,
      '#header' => $header,
    ];
    foreach ($this->settings() as $setting => $settingDef) {
      $form['sane']['events'][$setting]['label'] = [
        '#markup' => $settingDef['label'],
      ];
      foreach ($this->defaultSettings() as $key => $element) {
        $form['sane']['events'][$key]['label'] = [
          '#markup' => $element['#label'],
        ];
      }
      foreach ($this->topics() as $topic => $topicDef) {
        $form['sane']['events'][$setting][$topic] = [
          '#type' => 'checkbox',
          '#default_value' => $config->get($this->getSettingKey($setting, $topic)),
        ];
        if ($topicDef['default settings']) {
          foreach ($this->defaultSettings($roles, $config, $topic) as $key => $element) {
            $form['sane']['events'][$key][$topic] = $element;
            unset($form['sane']['events'][$key][$topic]['#description']);
          }
        }
      }
    }
    $form['sane']['events']['allow_subscription']['label'] = [
      '#markup' => $this->t('Allow subscription'),
    ];
    foreach ($this->topics() as $topic => $topicDef) {
      $form['sane']['events']['allow_subscription'][$topic] = [
        '#type' => 'select',
        '#options' => $roles,
        '#multiple' => TRUE,
        '#default_value' => $config->get($this->getSettingKey('allow_subscription', $topic)),
      ];
    }
    return [$this, 'submitContentTypeSettingsForm'];
  }

  /**
   * Submit callback.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitContentTypeSettingsForm(array &$form, FormStateInterface $form_state): void {
    $config_id = $form_state->getTemporaryValue(['sane', 'config_id']);
    $config = $this->configFactory->getEditable($config_id);
    $config
      ->set('events', $form_state->getValue('events'))
      ->save();
  }

  protected function buildContentForm(array &$form, FormStateInterface $form_state, ContentEntityInterface $entity, $topic): array {
    $config_id = $this->configId($entity->getEntityTypeId(), $entity->bundle());
    $config = $this->configFactory->get($config_id);
    $form_state->setTemporaryValue(['sane', 'topic'], $topic);
    $form_state->setTemporaryValue(['sane', 'entity'], $entity);

    $relevantRoles = array_intersect(
      $config->get($this->getSettingKey('access settings', $topic)),
      $this->currentUser->getRoles()
    );

    $form['sane'] = [
      '#type' => 'details',
      '#group' => isset($form['additional_settings']) ? 'additional_settings' : 'advanced',
      '#title' => $this->t('Sane'),
      '#weight' => 11,
      '#access' => !empty($relevantRoles),
      '#tree' => TRUE,
    ];
    /** @noinspection AdditionOperationOnArraysInspection */
    $form['sane'] += $this->defaultSettings([], $config, $topic, TRUE);

    return [$this, 'submitContentForm'];
  }

  /**
   * Submit callback.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitContentForm(array &$form, FormStateInterface $form_state): void {
    $info = $form_state->getBuildInfo();
    /** @var ContentEntityFormInterface $callback */
    $callback = $info['callback_object'];
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $callback->getEntity();
    $orgEntity = $form_state->getTemporaryValue(['sane', 'entity']);
    $values = $form_state->getValue('sane');

    switch ($form_state->getTemporaryValue(['sane', 'topic'])) {
      case self::TOPIC_CREATE:
        $create = $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_CREATE, $entity->getEntityTypeId(), $entity->bundle());
        $publish = $this->isPublished($entity) && $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_PUBLISH, $entity->getEntityTypeId(), $entity->bundle());
        $this->createContent($entity, $values['push'], $values['force'], $values['silent'], $create, $publish);
        break;

      case self::TOPIC_UPDATE:
        $update = $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_UPDATE, $entity->getEntityTypeId(), $entity->bundle());
        $newStatus = $this->isPublished($entity);
        /** @noinspection PhpUndefinedFieldInspection */
        $orgStatus = $this->isPublished($orgEntity);
        $publish = FALSE;
        $unpublish = FALSE;
        if ($newStatus !== $orgStatus) {
          if ($newStatus) {
            $publish = $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_PUBLISH, $entity->getEntityTypeId(), $entity->bundle());
          }
          else {
            $unpublish = $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_UNPUBLISH, $entity->getEntityTypeId(), $entity->bundle());
          }
        }
        $this->updateContent($entity, $values['push'], $values['force'], $values['silent'], $update, $publish, $unpublish);
        break;

      case self::TOPIC_DELETE:
        $delete = $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_DELETE, $entity->getEntityTypeId(), $entity->bundle());
        $unpublish = $this->isPublished($entity) && $this->getSetting(self::SETTING_CREATE_EVENT, self::TOPIC_UNPUBLISH, $entity->getEntityTypeId(), $entity->bundle());
        $this->deleteContent($entity, $values['push'], $values['force'], $values['silent'], $delete, $unpublish);
        break;
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $push
   * @param bool $force
   * @param bool $silent
   * @param bool $create
   * @param bool $publish
   */
  protected function createContent(ContentEntityInterface $entity, $push, $force, $silent, $create, $publish): void {
    if ($create) {
      $this->createEvent(self::TOPIC_CREATE, $entity->label(), new Payload($entity), $push, $force, $silent);
      // Make sure that a potential second event won't create duplicate
      // notifications.
      $silent = TRUE;
    }
    if ($publish) {
      $this->publishContent($entity, $push, $force, $silent);
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $push
   * @param bool $force
   * @param bool $silent
   * @param bool $update
   * @param bool $publish
   * @param bool $unpublish
   */
  protected function updateContent(ContentEntityInterface $entity, $push, $force, $silent, $update, $publish, $unpublish): void {
    if ($update) {
      $this->createEvent(self::TOPIC_UPDATE, $entity->label(), new Payload($entity), $push, $force, $silent);
      // Make sure that a potential second event won't create duplicate
      // notifications.
      $silent = TRUE;
    }
    if ($publish) {
      $this->publishContent($entity, $push, $force, $silent);
    }
    if ($unpublish) {
      $this->unpublishContent($entity, $push, $force, $silent);
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $push
   * @param bool $force
   * @param bool $silent
   * @param bool $delete
   * @param bool $unpublish
   */
  protected function deleteContent(ContentEntityInterface $entity, $push, $force, $silent, $delete, $unpublish): void {
    if ($unpublish) {
      $this->unpublishContent($entity, $push, $force, $silent);
      // Make sure that a potential second event won't create duplicate
      // notifications.
      $silent = TRUE;
    }
    if ($delete) {
      $this->createEvent(self::TOPIC_DELETE, $entity->label(), new Payload($entity), $push, $force, $silent);
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $push
   * @param bool $force
   * @param bool $silent
   */
  protected function publishContent(ContentEntityInterface $entity, $push, $force, $silent): void {
    $this->createEvent(self::TOPIC_PUBLISH, $entity->label(), new Payload($entity), $push, $force, $silent);
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $push
   * @param bool $force
   * @param bool $silent
   */
  protected function unpublishContent(ContentEntityInterface $entity, $push, $force, $silent): void {
    $this->createEvent(self::TOPIC_UNPUBLISH, $entity->label(), new Payload($entity), $push, $force, $silent);
  }

}
