<?php


namespace Drupal\sane_content\Plugin\Sane;

use Drupal\Core\StringTranslation\StringTranslationTrait;

trait ContentSettingsTrait {

  use StringTranslationTrait;

  /**
   * @param string $entityTypeId
   * @param string $bundle
   *
   * @return string
   */
  protected function configId($entityTypeId, $bundle = NULL): string {
    return 'sane.settings.content.' . $entityTypeId . '.' . ($bundle ?? $entityTypeId);
  }

  /**
   * @param string $key
   * @param string $topic
   *
   * @return string
   */
  protected function getSettingKey($key, $topic): string {
    return implode('.', ['events', $key, $topic]);
  }

  /**
   * @return array
   */
  protected function settings(): array {
    return [
      self::SETTING_CREATE_EVENT => [
        'label' => $this->t('Create event'),
      ],
    ];
  }

  /**
   * @param array $roles
   * @param \Drupal\Core\Config\ImmutableConfig $config
   * @param string $topic
   * @param bool $forEdit
   *
   * @return array
   */
  protected function defaultSettings($roles = [], $config = NULL, $topic = NULL, $forEdit = FALSE): array {
    return [
      'access settings' => [
        '#type' => 'select',
        '#options' => $roles,
        '#multiple' => TRUE,
        '#default_value' => $topic ? $config->get($this->getSettingKey('access settings', $topic)) : [],
        '#label' => $this->t('Access to settings'),
        '#title' => $this->t('Access to settings'),
        '#title_display' => 'hidden',
        '#access' => !$forEdit,
      ],
      'push' => [
        '#type' => 'checkbox',
        '#default_value' => $topic ? ($config->get($this->getSettingKey('push', $topic)) ?? TRUE) : TRUE,
        '#label' => $this->t('Default value for push'),
        '#title' => $this->t('Create push notification'),
        '#title_display' => $forEdit ? 'after' : 'hidden',
      ],
      'force' => [
        '#type' => 'checkbox',
        '#default_value' => $topic ? ($config->get($this->getSettingKey('force', $topic)) ?? FALSE) : FALSE,
        '#label' => $this->t('Default value for force'),
        '#title' => $this->t('Force push even if this creates duplicate notification'),
        '#title_display' => $forEdit ? 'after' : 'hidden',
      ],
      'silent' => [
        '#type' => 'checkbox',
        '#default_value' => $topic ? ($config->get($this->getSettingKey('silent', $topic)) ?? FALSE) : FALSE,
        '#label' => $this->t('Default value for silent'),
        '#title' => $this->t('Make this silent and do not create any notifications'),
        '#title_display' => $forEdit ? 'after' : 'hidden',
        '#description' => $this->t('If this is checked, the other options have no effect anymore.'),
      ],
    ];
  }

  /**
   * @return array
   */
  protected function topics(): array {
    return [
      self::TOPIC_CREATE => [
        'label' => $this->t('Create'),
        'default settings' => TRUE,
      ],
      self::TOPIC_UPDATE => [
        'label' => $this->t('Update'),
        'default settings' => TRUE,
      ],
      self::TOPIC_DELETE => [
        'label' => $this->t('Delete'),
        'default settings' => TRUE,
      ],
      self::TOPIC_PUBLISH => [
        'label' => $this->t('Publish'),
        'default settings' => FALSE,
      ],
      self::TOPIC_UNPUBLISH => [
        'label' => $this->t('Unpublish'),
        'default settings' => FALSE,
      ],
    ];
  }

}
