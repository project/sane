<?php

namespace Drupal\sane_log\EventSubscriber;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\sane_log\Event\LogEvent;
use Drupal\sane_log\LogEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Log event subscriber.
 */
class DefaultLog implements EventSubscriberInterface {

  /**
   * New log item event handler.
   *
   * @param \Drupal\sane_log\Event\LogEvent $event
   */
  public function onNewLog(LogEvent $event): void {
    if ($event->getPayload()->getLevel() <= RfcLogLevel::ERROR) {
      $event->setRelevant();
    }
    if ($event->isRelevant() && $event->getPayload()->getMessage() === 'Illegal choice %choice in %name element.') {
      $event->setIrrelevant();
    }
    if ($event->isRelevant() && strpos($event->getPayload()->getMessage(), 'Import of string ') === 0) {
      $event->setIrrelevant();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      LogEvents::LOG => ['onNewLog'],
    ];
  }

}
