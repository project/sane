<?php

namespace Drupal\sane_log;

/**
 * Contains all events thrown by the SANE log module.
 */
final class LogEvents {

  public const LOG = 'log';

}
