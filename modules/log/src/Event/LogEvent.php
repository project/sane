<?php

namespace Drupal\sane_log\Event;

use Drupal\sane_log\Payload;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class LogEvent
 *
 * @package Drupal\sane_log\Event
 */
class LogEvent extends Event {

  /**
   * @var bool
   */
  protected $status = FALSE;

  /**
   * @var \Drupal\sane_log\Payload
   */
  protected $payload;

  /**
   * LogEvent constructor.
   *
   * @param \Drupal\sane_log\Payload $payload
   */
  public function __construct(Payload $payload) {
    $this->payload = $payload;
  }

  /**
   * Marks this event relevant.
   */
  public function setRelevant(): self {
    $this->status = TRUE;
    return $this;
  }

  /**
   * Marks this event irrelevant.
   */
  public function setIrrelevant(): self {
    $this->status = FALSE;
    return $this;
  }

  /**
   * @return bool
   */
  public function isRelevant(): bool {
    return $this->status;
  }

  /**
   * @return \Drupal\sane_log\Payload
   */
  public function getPayload(): Payload {
    return $this->payload;
  }

}
