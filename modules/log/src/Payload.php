<?php

namespace Drupal\sane_log;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\sane\Entity\Event;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadBase;
use Drupal\sane\PayloadInterface;

class Payload extends PayloadBase {

  /**
   * @var int
   */
  private $level;

  /**
   * @var string
   */
  protected $message;

  /**
   * @var array
   */
  protected $context;

  /**
   * Content constructor.
   *
   * @param int $level
   * @param string $message
   * @param array $context
   */
  public function __construct($level, $message, $context) {
    $this->level = $level;
    $this->message = $message;
    $this->context = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventReference(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriptionReference(EventInterface $event): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function prepareArray(): array {
    return [
      'level' => $this->level,
      'message' => $this->message,
      'context' => $this->context,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromArray(array $payload): PayloadInterface {
    return new static(
      $payload['level'],
      $payload['message'],
      $payload['context']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): ContentEntityInterface {
    /** @var Event $entity */
    $entity = Event::create([
      'plugin' => 'log',
      'topic' => '',
      'label' => '',
      'payload' => $this,
      'push' => TRUE,
      'force' => TRUE,
      'silent' => FALSE,
    ]);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccess($uid): bool {
    return FALSE;
  }

  /**
   * @return int
   */
  public function getLevel(): int {
    return $this->level;
  }

  /**
   * @return string
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * Helper function for Alerta chennel to fill the values for the payload.
   *
   * @param array $data
   */
  public function formatAlerta(&$data) {
    $subject = strip_tags((string) t($this->message, $this->context['placeholders'], ['langcode' => 'en',]));
    $host = parse_url($this->context['request_uri'], PHP_URL_HOST);
    $event = mb_substr($this->context['channel'], 0, 64);

    $data['resource'] = hash('md5', $host . $subject);
    $data['event'] = $event;
    $data['service'] = [$host];
    $data['text'] = explode("\n", str_replace('\\', '/', $subject))[0];
    $data['rawData'] = [
      'uid' => $this->context['uid'],
      'type' => $event,
      'severity' => $this->level,
      'link' => $this->context['link'],
      'location' => $this->context['request_uri'],
      'referer' => $this->context['referer'],
      'hostname' => mb_substr($this->context['ip'], 0, 128),
      'timestamp' => $this->context['timestamp'],
      'message' => $subject,
      'backtrace' => $this->context['backtrace'],
    ];
  }

}
