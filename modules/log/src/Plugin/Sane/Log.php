<?php

namespace Drupal\sane_log\Plugin\Sane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadInterface;
use Drupal\sane\PluginBase;
use Drupal\sane_log\Payload;

/**
 * Plugin implementation of the sane.
 *
 * @Sane(
 *   id = "log",
 *   label = @Translation("Log"),
 *   description = @Translation("Manages log events.")
 * )
 */
class Log extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function assertPayload(PayloadInterface $payload): bool {
    return $payload instanceof Payload;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedSubscriptions($roles): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(&$form, FormStateInterface $form_state) {
    // TODO: Implement buildForm() method.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl(EventInterface $event): Url {
    return Url::fromRoute('<front>');
  }

  /**
   * @param string $topic
   * @param string $message
   * @param \Drupal\sane_log\Payload $payload
   *
   * @return \Drupal\sane\Entity\EventInterface
   */
  public function createLogEvent($topic, $message, Payload $payload): EventInterface {
    return $this->createEvent(mb_substr($topic, 0, 32), $message, $payload, TRUE, TRUE, FALSE);
  }

}
