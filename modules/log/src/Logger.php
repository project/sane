<?php

namespace Drupal\sane_log;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\sane\PluginManager;
use Drupal\sane_log\Event\LogEvent;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Logger service to write messages and exceptions to an external service.
 */
class Logger extends LoggerChannel {
  use RfcLoggerTrait;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * @var \Drupal\sane\PluginManager
   */
  protected $pluginManager;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   * @param \Drupal\sane\PluginManager $plugin_manager
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   */
  public function __construct(LogMessageParserInterface $parser, PluginManager $plugin_manager, EventDispatcherInterface $event_dispatcher) {
    parent::__construct('sane');
    $this->parser = $parser;
    $this->pluginManager = $plugin_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    try {
      /** @var \Drupal\sane_log\Plugin\Sane\Log $plugin */
      $plugin = $this->pluginManager->createInstance('log');
    }
    catch (PluginException $e) {
      // This happens during installation, when the new plugin will not be
      // known yet and then we can ignore.
      return;
    }
    unset($context['backtrace']);
    $context['placeholders'] = $this->parser->parseMessagePlaceholders($message, $context);
    $context['backtrace'] = explode("\n", $context['placeholders']['@backtrace_string'] ?? '');
    unset($context['placeholders']['@backtrace_string']);
    $payload = new Payload($level, $message, $context);

    $event = new LogEvent($payload);
    $this->eventDispatcher->dispatch(LogEvents::LOG, $event);
    if ($event->isRelevant()) {
      try {
        $plugin->createLogEvent($context['channel'], $message, $payload);
      }
      catch (Exception $e) {
        // We have to ignore any error here, other we are going to run in circles.
      }
    }
  }

}
