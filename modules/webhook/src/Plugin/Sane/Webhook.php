<?php

namespace Drupal\sane_webhook\Plugin\Sane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadInterface;
use Drupal\sane\PluginBase;
use Drupal\sane_webhook\Payload;

/**
 * Plugin implementation of the sane.
 *
 * @Sane(
 *   id = "webhook",
 *   label = @Translation("Webhook"),
 *   description = @Translation("Provides a webhook to create events.")
 * )
 */
class Webhook extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function assertPayload(PayloadInterface $payload): bool {
    return $payload instanceof Payload;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedSubscriptions($roles): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(&$form, FormStateInterface $form_state) {
    // TODO: Implement buildForm() method.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl(EventInterface $event): Url {
    return Url::fromRoute('<front>');
  }

  public function createWebhookEvent($agent, $label, Payload $payload): EventInterface {
    return $this->createEvent($agent, $label, $payload);
  }

}
