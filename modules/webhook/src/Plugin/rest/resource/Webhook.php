<?php

namespace Drupal\sane_webhook\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\sane\PluginManager;
use Drupal\sane_webhook\Payload;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Represents Example records as resources.
 *
 * @RestResource (
 *   id = "sane_webhook",
 *   label = @Translation("SANE Event"),
 *   uri_paths = {
 *     "canonical" = "/api/sane-webhook/{id}",
 *     "https://www.drupal.org/link-relations/create" = "/api/sane-webhook"
 *   }
 * )
 */
class Webhook extends ResourceBase implements DependentPluginInterface {

  /**
   * @var \Drupal\sane_webhook\Plugin\Sane\Webhook
   */
  protected $plugin;

  /**
   * @inheritDoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, PluginManager $plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->plugin = $plugin_manager->createInstance('webhook');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('sane.plugin.manager')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $record
   *   Data to write into the database.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The HTTP response object.
   */
  public function post($record): Response {
    if ($this->validate($record)) {
      /** @var Payload $payload */
      $payload = Payload::fromArray($record);
      $this->plugin->createWebhookEvent($payload);
      return new Response('ok', 201);
    }
    return new Response('Invalid record', 400);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

  protected function validate(&$record) {
    foreach (['extid', ['recipients', 'recipient'], 'label', 'message'] as $item) {
      if (is_array($item)) {
        foreach ($item as $subitem) {
          if (isset($record[$subitem])) {
            continue 2;
          }
        }
        return FALSE;
      }
      if (!isset($record[$item])) {
        return FALSE;
      }
    }

    if (!is_string($record['label']) || !is_string($record['message'])) {
      return FALSE;
    }
    if (!isset($record['recipients'])) {
      $record['recipients'] = [];
    }
    elseif (!is_array($record['recipients'])) {
      return FALSE;
    }
    if (isset($record['recipient'])) {
      if (is_string($record['recipient'])) {
        $record['recipients'][] = $record['recipient'];
        unset($record['recipient']);
      }
      else {
        return FALSE;
      }
    }

    return TRUE;
  }

}
