<?php

namespace Drupal\sane_webhook;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\node\Entity\Node;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\PayloadBase;
use Drupal\sane\PayloadInterface;

class Payload extends PayloadBase {

  /**
   * @var string
   */
  protected $extId;

  /**
   * @var array
   */
  protected $recipients;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $message;

  /**
   * Content constructor.
   *
   * @param string $extId
   */
  public function __construct($extId) {
    $this->extId = $extId;
  }

  /**
   * @param string $recipient
   *
   * @return \Drupal\sane_webhook\Payload
   */
  public function addRecipient($recipient): Payload {
    $this->recipients[] = $recipient;
    return $this;
  }

  /**
   * @param string $label
   *
   * @return \Drupal\sane_webhook\Payload
   */
  public function setLabel($label): Payload {
    $this->label = $label;
    return $this;
  }

  /**
   * @param string $message
   *
   * @return \Drupal\sane_webhook\Payload
   */
  public function setMessage($message): Payload {
    $this->message = $message;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventReference(): string {
    return implode('-', ['webhook', $this->extId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriptionReference(EventInterface $event): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function prepareArray(): array {
    return [
      'extid' => $this->extId,
      'recipients' => $this->recipients ?? [],
      'label' => $this->label,
      'message' => $this->message,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromArray(array $payload): PayloadInterface {
    $item = new static($payload['extid']);
    foreach ($payload['recipients'] as $recipient) {
      $item->addRecipient($recipient);
    }
    $item
      ->setLabel($payload['label'])
      ->setMessage($payload['message']);
    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): ContentEntityInterface {
    /** @var \Drupal\node\NodeInterface $entity */
    $entity = Node::create([
      'title' => $this->label,
      'body' => $this->message,
    ]);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccess($uid): bool {
    if (empty($this->recipients)) {
      return TRUE;
    }

    try {
      $storage = Drupal::entityTypeManager()->getStorage('user');
      foreach ($this->recipients as $recipient) {
        $users = $storage->getQuery('OR')
          ->condition('name', $recipient)
          ->condition('mail', $recipient)
          ->execute();
        if (!empty($users)) {
          return TRUE;
        }
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      // TODO: Log this exception.
    }
    catch (PluginNotFoundException $e) {
      // TODO: Log this exception.
    }
    return FALSE;
  }

}
