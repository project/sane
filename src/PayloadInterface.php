<?php

namespace Drupal\sane;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\sane\Entity\EventInterface;

/**
 * Interface for sane payload.
 */
interface PayloadInterface {

  /**
   * @return string
   */
  public function getEventReference(): string;

  /**
   * @param \Drupal\sane\Entity\EventInterface $event
   *
   * @return string
   */
  public function getSubscriptionReference(EventInterface $event): string;

  /**
   * @return array
   */
  public function prepareArray(): array;

  /**
   * @param array $payload
   *
   * @return \Drupal\sane\PayloadInterface
   */
  public static function createFromArray(array $payload): PayloadInterface;

  /**
   * @return \Drupal\Core\Entity\ContentEntityInterface
   */
  public function getEntity(): ContentEntityInterface;

  /**
   * @param int $uid
   *
   * @return bool
   */
  public function hasAccess($uid): bool;

}
