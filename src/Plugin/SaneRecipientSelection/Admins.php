<?php

namespace Drupal\sane\Plugin\SaneRecipientSelection;

use Drupal\sane\PayloadInterface;
use Drupal\sane\RecipientSelectionBase;

/**
 * Plugin implementation of the sane.
 *
 * @SaneRecipientSelection(
 *   id = "admins",
 *   label = @Translation("All admins"),
 *   description = @Translation("Selects all active users with the admin role.")
 * )
 */
class Admins extends RecipientSelectionBase {

  /**
   * {@inheritdoc}
   */
  public function getRecipients(PayloadInterface $payload): array {
    // TODO: Select all admins.
    return [];
  }

}
