<?php

namespace Drupal\sane\Plugin\SaneRecipientSelection;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\sane\PayloadInterface;
use Drupal\sane\RecipientSelectionBase;

/**
 * Plugin implementation of the sane.
 *
 * @SaneRecipientSelection(
 *   id = "default",
 *   label = @Translation("All active users"),
 *   description = @Translation("Selects all active users by default.")
 * )
 */
class AllActiveUsers extends RecipientSelectionBase {

  /**
   * {@inheritdoc}
   */
  public function getRecipients(PayloadInterface $payload): array {
    try {
      return $this->entityTypeManager->getStorage('user')->getQuery()
        ->condition('status', 1)
        ->execute();
    }
    catch (InvalidPluginDefinitionException $e) {
      // TODO: Handle exception.
    }
    catch (PluginNotFoundException $e) {
      // TODO: Handle exception.
    }
    return [];
  }

}
