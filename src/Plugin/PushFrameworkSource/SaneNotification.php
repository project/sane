<?php

namespace Drupal\sane\Plugin\PushFrameworkSource;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\push_framework\Annotation\SourcePlugin;
use Drupal\push_framework\ChannelPluginInterface;
use Drupal\push_framework\SourceItem;
use Drupal\push_framework\SourcePluginInterface;
use Drupal\push_framework\SourceBase;
use Drupal\sane\Entity\Notification;
use Drupal\sane\Entity\NotificationAction;
use Drupal\user\UserInterface;

/**
 * Plugin implementation of the push framework source.
 *
 * @SourcePlugin(
 *   id = "sane_notification",
 *   label = @Translation("SANE Notification"),
 *   description = @Translation("Provides all the notifications that need to be pushed.")
 * )
 */
class SaneNotification extends SourceBase {

  /**
   * {@inheritdoc}
   */
  public function getAllItemsForPush(): array {
    $items = [];
    try {
      $notifications = $this->entityTypeManager->getStorage('sane_notification')
        ->loadByProperties([
          'delivered' => 0,
          'seen' => 0,
          'redundant' => 0,
        ]);
      foreach ($notifications as $notification) {
        $items[] = new SourceItem($this, $notification->id(), $notification->uid());
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      // TODO: Log this exception.
    }
    catch (PluginNotFoundException $e) {
      // TODO: Log this exception.
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectAsEntity($oid): ContentEntityInterface {
    /** @var \Drupal\sane\Entity\NotificationInterface $notification */
    $notification = Notification::load($oid);
    return $notification->event()->getPayload()->getEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function confirmAttempt($oid, UserInterface $user, ChannelPluginInterface $channelPlugin, $result): SourcePluginInterface {
    $action = NotificationAction::create([
      'notification' => $oid,
      'success' => $result === ChannelPluginInterface::RESULT_STATUS_SUCCESS,
      'payload' => [
        'channel plugin' => $channelPlugin->getPluginId(),
      ],
    ]);
    try {
      $action->save();
    }
    catch (EntityStorageException $e) {
      // TODO: Log this exception.
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function confirmDelivery($oid, UserInterface $user): SourcePluginInterface {
    /** @var \Drupal\sane\Entity\NotificationInterface $notification */
    $notification = Notification::load($oid);
    $notification->markDelivered();
    return $this;
  }

}
