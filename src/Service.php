<?php

namespace Drupal\sane;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserDataInterface;

/**
 * Logger service to write messages and exceptions to an external service.
 */
class Service {

  use StringTranslationTrait;

  /**
   * @var \Drupal\sane\PluginManager
   */
  protected $pluginManager;

  /**
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * @var \Drupal\sane\Query
   */
  protected $query;

  /**
   * @param \Drupal\sane\PluginManager $plugin_manager
   * @param \Drupal\user\UserDataInterface $user_data
   * @param \Drupal\sane\Query $query
   */
  public function __construct(PluginManager $plugin_manager, UserDataInterface $user_data, Query $query) {
    $this->pluginManager = $plugin_manager;
    $this->userData = $user_data;
    $this->query = $query;
  }

  /**
   * @return \Drupal\sane\PluginInterface[]
   */
  public function getPluginInstances(): array {
    $instances = [];
    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      try {
        $instances[$id] = $this->pluginManager->createInstance($id);
      }
      catch (PluginException $e) {
        // We can savely ignore this here.
      }
    }
    return $instances;
  }

  /**
   * @param array $form
   * @param array|string $callback
   */
  protected function addSubmitHandler(&$form, $callback): void {
    $callback = is_string($callback) ? [$this, $callback] : $callback;
    if (isset($form['actions']['submit']['#submit'])) {
      foreach (array_keys($form['actions']) as $action) {
        if ($action !== 'preview'
          && isset($form['actions'][$action]['#type'])
          && $form['actions'][$action]['#type'] === 'submit') {
          $form['actions'][$action]['#submit'][] = $callback;
        }
      }
    }
    else {
      $form['#submit'][] = $callback;
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function buildForm(&$form, FormStateInterface $form_state): void {
    $submitHandler = [];
    foreach ($this->getPluginInstances() as $plugin) {
      if ($result = $plugin->buildForm($form, $form_state)) {
        $submitHandler[] = $result;
      }
    }
    foreach ($submitHandler as $item) {
      $this->addSubmitHandler($form, $item);
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function buildUserSubscriptionForm(&$form, FormStateInterface $form_state): void {
    /** @var AccountProxyInterface $account */
    $account = $form_state->getFormObject()->getEntity();
    $roles = $account->getRoles();
    $checkboxes = [];
    foreach ($this->getPluginInstances() as $plugin) {
      foreach ($plugin->getSupportedSubscriptions($roles) as $key => $title) {
        $checkboxes[$key] = [
          '#type' => 'checkbox',
          '#title' => $title,
          '#default_value' => $this->userData->get('sane', $account->id(), $key),
        ];
      }
    }
    if (!empty($checkboxes)) {
      $form['sane'] = [
        '#type' => 'details',
        '#title' => $this->t('Subscriptions'),
        '#open' => FALSE,
        '#tree' => TRUE,
        '#weight' => 9,
      ];
      /** @noinspection AdditionOperationOnArraysInspection */
      $form['sane'] += $checkboxes;
      $this->addSubmitHandler($form, 'submitUserSubscriptionForm');
    }
  }

  /**
   * Submit callback.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitUserSubscriptionForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Session\AccountProxyInterface $account */
    $account = $form_state->getFormObject()->getEntity();
    foreach ($form_state->getValue('sane') as $key => $value) {
      $this->userData->set('sane', $account->id(), $key, (int) $value);
    }
  }

  /**
   * Create all outstanding notifications.
   */
  public function createNotifications(): void {
    foreach ($this->getPluginInstances() as $plugin) {
      $plugin->createNotifications();
    }
  }

  /**
   * @param \Drupal\sane\PayloadInterface $payload
   */
  public function markSeen($payload): void {
    foreach ($this->query->findNotificationsForCurrentUser($payload) as $notification) {
      $notification->markSeen();
    }
  }

}
