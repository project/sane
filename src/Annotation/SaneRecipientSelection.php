<?php

namespace Drupal\sane\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines sane recipient selection annotation object.
 *
 * @Annotation
 */
class SaneRecipientSelection extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
