<?php

namespace Drupal\sane\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\sane\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the node entity type.
 *
 * @see \Drupal\node\Entity\Node
 * @ingroup node_access
 */
class EventAccess extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * @var \Drupal\sane\Query
   */
  protected $query;

  /**
   * @inheritDoc
   */
  public function __construct(EntityTypeInterface $entity_type, Query $query) {
    parent::__construct($entity_type);
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('sane.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($entity instanceof EventInterface && $this->query->findEventNotificationsForCurrentUser($entity)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
