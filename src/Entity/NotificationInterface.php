<?php

namespace Drupal\sane\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface NotificationInterface extends ContentEntityInterface {

  /**
   * @return int
   */
  public function uid(): int;

  /**
   * @return \Drupal\sane\Entity\EventInterface
   */
  public function event(): EventInterface;

  /**
   * @return \Drupal\sane\Entity\NotificationInterface
   */
  public function markSeen(): NotificationInterface;

  /**
   * @return \Drupal\sane\Entity\NotificationInterface
   */
  public function markDelivered(): NotificationInterface;

  /**
   * @param \Drupal\sane\Entity\NotificationInterface $notification
   *
   * @return \Drupal\sane\Entity\NotificationInterface
   */
  public function setSuccessor(NotificationInterface $notification): NotificationInterface;

}
