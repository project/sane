<?php

namespace Drupal\sane\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\sane\PayloadInterface;

interface EventInterface extends ContentEntityInterface {

  /**
   * @return string
   */
  public function getPluginId(): string;

  /**
   * @return \Drupal\sane\PayloadInterface
   */
  public function getPayload(): PayloadInterface;

  /**
   * @return string
   */
  public function getTopic(): string;

  /**
   * @return bool
   */
  public function isPush(): bool;

  /**
   * @return bool
   */
  public function isForce(): bool;

  /**
   * @return \Drupal\sane\Entity\EventInterface
   */
  public function setProcessed(): EventInterface;

}
