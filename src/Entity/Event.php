<?php

namespace Drupal\sane\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\sane\PayloadBase;
use Drupal\sane\PayloadInterface;

/**
 * Defines the sane event entity class.
 *
 * @ContentEntityType(
 *   id = "sane_event",
 *   label = @Translation("Event"),
 *   internal = TRUE,
 *   handlers = {
 *     "view_builder" = "Drupal\sane\Entity\EventView",
 *     "access" = "Drupal\sane\Entity\EventAccess",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "sane_event",
 *   data_table = "sane_event",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "type" = "topic",
 *     "uuid" = "uuid",
 *     "status" = "processed"
 *   },
 *   links = {
 *     "canonical" = "/sane/event/{sane_event}",
 *   },
 * )
 */
class Event extends ContentEntityBase implements EventInterface {

  use EntityChangedTrait;

  /**
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   * @param array $values
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    if (!empty($values['payload'])) {
      $values['reference'] = $values['payload']->getEventReference();
      $values['payload'] = json_encode($values['payload']->toArray());
    }
    if (!empty($values['silent'])) {
      $values['processed'] = TRUE;
    }
    parent::preCreate($storage, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->get('plugin')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPayload(): PayloadInterface {
    $payload = $this->get('payload')->value;
    if (is_string($payload)) {
      $payload = json_decode($payload, TRUE);
    }
    return PayloadBase::fromArray($payload);
  }

  /**
   * {@inheritdoc}
   */
  public function getTopic(): string {
    return $this->get('topic')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isPush(): bool {
    return (bool) $this->get('push')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isForce(): bool {
    return (bool) $this->get('force')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProcessed(): EventInterface {
    try {
      $this
        ->set('processed', TRUE)
        ->save();
    }
    catch (EntityStorageException $e) {
      // TODO: Log this exception.
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['plugin'] = BaseFieldDefinition::create('string')
      ->setLabel('Plugin')
      ->setRequired(TRUE)
      ->setSetting('max_length', 32);
    $fields['topic'] = BaseFieldDefinition::create('string')
      ->setLabel('Topic')
      ->setRequired(TRUE)
      ->setSetting('max_length', 32);
    $fields['reference'] = BaseFieldDefinition::create('string')
      ->setLabel('Reference')
      ->setRequired(TRUE)
      ->setSetting('max_length', 64);
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel('Label')
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);
    $fields['payload'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Payload')
      ->setRequired(TRUE);
    $fields['push'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Push')
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);
    $fields['force'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Force')
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);
    $fields['silent'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Silent')
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);
    $fields['processed'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Processed')
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel('Created')
      ->setRequired(TRUE);
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel('Change')
      ->setRequired(TRUE);

    return $fields;
  }

}
