<?php

namespace Drupal\sane\Entity;

use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\sane\PluginManager;
use Drupal\sane\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * View builder handler for sane events.
 */
class EventView extends EntityViewBuilder {

  /**
   * @var \Drupal\Core\DrupalKernel
   */
  protected $kernel;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\sane\Query
   */
  protected $query;

  /**
   * @var \Drupal\sane\PluginManager
   */
  protected $pluginManager;

  /**
   * EventView constructor.
   *
   * @param \Drupal\Core\DrupalKernelInterface $kernel
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\sane\Query $query
   * @param \Drupal\sane\PluginManager $plugin_manager
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   * @param \Drupal\Core\Theme\Registry $theme_registry
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   */
  public function __construct(DrupalKernelInterface $kernel, Request $request, Query $query, PluginManager $plugin_manager, EntityTypeInterface $entity_type, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL, EntityDisplayRepositoryInterface $entity_display_repository = NULL) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->kernel = $kernel;
    $this->request = $request;
    $this->query = $query;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('kernel'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('sane.query'),
      $container->get('sane.plugin.manager'),
      $entity_type,
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $build) {
    $session = $this->request->getSession();
    if ($session === NULL) {
      return;
    }
    /** @var \Drupal\sane\Entity\EventInterface $event */
    $event = $build['#sane_event'];
    $plugin = $this->pluginManager->createInstance($event->getPluginId());
    foreach ($this->query->findEventNotificationsForCurrentUser($event) as $notification) {
      try {
        $notification
          ->markSeen()
          ->save();
      }
      catch (EntityStorageException $e) {
        // TODO: handle exception.
      }
    }

    $response = new RedirectResponse($plugin->getRedirectUrl($event)->toString());
    $session->save();
    $response->prepare($this->request);
    $this->kernel->terminate($this->request, $response);
    $response->send();
    exit;
  }

}
