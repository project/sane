<?php

namespace Drupal\sane\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface NotificationActionInterface extends ContentEntityInterface {

}
