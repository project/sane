<?php

namespace Drupal\sane\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sane\RecipientSelectionManager;
use Drupal\sane\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Subscription Audit Notification Event settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * @var \Drupal\sane\Service
   */
  protected $sane;

  /**
   * @var \Drupal\sane\RecipientSelectionManager
   */
  protected $recipientSelectionManager;

  /**
   * Settings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\sane\Service $sane
   * @param \Drupal\sane\RecipientSelectionManager $recipient_selection_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, Service $sane, RecipientSelectionManager $recipient_selection_manager) {
    parent::__construct($config_factory);
    $this->sane = $sane;
    $this->recipientSelectionManager = $recipient_selection_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('sane.service'),
      $container->get('sane.recipient.selection.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sane_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['sane.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['recipient_selection_plugin'] = [
      '#type' => 'details',
      '#title' => $this->t('Recipient selection plugins'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    foreach ($this->sane->getPluginInstances() as $id => $plugin) {
      $form['recipient_selection_plugin'][$id] = [
        '#type' => 'select',
        '#title' => $plugin->label(),
        '#options' => $this->recipientSelectionManager->pluginListForSelect(),
        '#default_value' => $this->config('sane.settings')
          ->get('recipient_selection_plugin.' . $id),
        '#required' => TRUE,
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Config\Config $config */
    if ($config = $this->config('sane.settings')) {
      $config
        ->set('recipient_selection_plugin', $form_state->getValue('recipient_selection_plugin'))
        ->save();
    }
    parent::submitForm($form, $form_state);
  }

}
