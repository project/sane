<?php

namespace Drupal\sane;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginBase as CorePluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\sane\Entity\Event;
use Drupal\sane\Entity\EventInterface;
use Drupal\sane\Entity\Notification;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for sane plugins.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\sane\RecipientSelectionInterface
   */
  protected $recipientSelectionPlugin;

  /**
   * @var \Drupal\sane\Query
   */
  protected $query;

  /**
   * PluginBase constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\user\UserDataInterface $user_data
   * @param \Drupal\sane\RecipientSelectionManager $recipient_selection_manager
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\sane\Query $query
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info, ConfigFactoryInterface $config_factory, UserDataInterface $user_data, RecipientSelectionManager $recipient_selection_manager, AccountProxyInterface $current_user, RouteMatchInterface $route_match, Query $query) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
    $this->configFactory = $config_factory;
    $this->userData = $user_data;
    $this->currentUser = $current_user;
    $this->routeMatch = $route_match;
    $this->query = $query;
    try {
      $this->recipientSelectionPlugin = $recipient_selection_manager->createInstance($config_factory->get('sane.settings')
        ->get('recipient_selection_plugin.' . $this->getPluginId()));
    }
    catch (PluginException $e) {
      // This may fail, i.e. during enable/disable module.
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('config.factory'),
      $container->get('user.data'),
      $container->get('sane.recipient.selection.manager'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('sane.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  final public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  final public function createNotifications(): void {
    /** @var \Drupal\sane\Entity\EventInterface $event */
    foreach ($this->getUnprocessedEvents() as $event) {
      /** @var \Drupal\sane\PayloadInterface $payload */
      $payload = $event->getPayload();
      $subscribers = $this->getSubscribers($event);
      $recipients = ($event->isPush() && $this->recipientSelectionPlugin !== NULL) ?
        $this->recipientSelectionPlugin->getRecipients($payload) :
        [];
      foreach ($subscribers as $subscriber) {
        $this->createNotification($event, 'subscription', $subscriber);
      }
      foreach ($recipients as $recipient) {
        if (!in_array($recipient, $subscribers, TRUE)) {
          $this->createNotification($event, 'push', $recipient);
        }
      }
      $event->setProcessed();
    }
  }

  /**
   * @param string $topic
   * @param string $label
   * @param \Drupal\sane\PayloadInterface $payload
   * @param bool $push
   * @param bool $force
   * @param bool $silent
   *
   * @return \Drupal\sane\Entity\EventInterface
   */
  protected function createEvent($topic, $label, PayloadInterface $payload, $push = TRUE, $force = FALSE, $silent = FALSE): EventInterface {
    if (!$this->assertPayload($payload)) {
      // TODO: Log "Invalid payload provided.".
      return NULL;
    }
    /** @var EventInterface $event */
    $event = Event::create([
      'plugin' => $this->getPluginId(),
      'topic' => $topic,
      'label' => $label,
      'payload' => $payload,
      'push' => $push,
      'force' => $force,
      'silent' => $silent,
    ]);
    try {
      $event->save();
    } catch (EntityStorageException $e) {
      // TODO: Log the issue
      return NULL;
    }
    return $event;
  }

  /**
   *
   * @param \Drupal\sane\Entity\EventInterface $event
   *
   * @return int[]
   */
  protected function getSubscribers(EventInterface $event): array {
    $reference = $event->getPayload()->getSubscriptionReference($event);
    $uids = [];
    foreach ($this->userData->get('sane', NULL, $reference) as $uid => $flag) {
      if ($flag) {
        $uids[] = $uid;
      }
    }
    return $uids;
  }

  /**
   * @param \Drupal\sane\Entity\EventInterface $event
   * @param string $trigger
   * @param int $uid
   *
   * @return \Drupal\sane\Entity\NotificationInterface[]
   */
  protected function createNotification(EventInterface $event, $trigger, $uid): array {
    if (!$event->getPayload()->hasAccess($uid)) {
      return [];
    }
    $existingNotifications = $this->query->findSimilarEventNotifications($event, $uid);
    if (!empty($existingNotifications) && !$event->isForce()) {
      // There is an existing notification which is undelivered, so we do not create a new one.
      return $existingNotifications;
    }
    /** @var NotificationInterface $notification */
    $notification = Notification::create([
      'event' => $event,
      'trigger' => $trigger,
      'uid' => $uid,
    ]);
    try {
      $notification->save();
    } catch (EntityStorageException $e) {
      // TODO: Log the issue
      return [];
    }
    // Mark existing notifications as redundant.
    foreach ($existingNotifications as $existingNotification) {
      $existingNotification->setSuccessor($notification);
    }
    return [$notification];
  }

  /**
   * @return \Drupal\sane\Entity\Event[]
   */
  protected function getUnprocessedEvents(): array {
    try {
      $events = $this->entityTypeManager->getStorage('sane_event')
        ->loadByProperties([
          'plugin' => $this->getPluginId(),
          'processed' => 0,
        ]);
      ksort($events);
      return $events;
    }
    catch (InvalidPluginDefinitionException $e) {
      // TODO: handle exception.
    }
    catch (PluginNotFoundException $e) {
      // TODO: handle exception.
    }
    return [];
  }

}
