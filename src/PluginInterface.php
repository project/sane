<?php

namespace Drupal\sane;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sane\Entity\EventInterface;

/**
 * Interface for sane plugins.
 */
interface PluginInterface extends PluginInspectionInterface {

  /**
   * @param \Drupal\sane\PayloadInterface $payload
   *
   * @return bool
   */
  public function assertPayload(PayloadInterface $payload): bool;

  /**
   * @param array $roles
   *
   * @return array
   */
  public function getSupportedSubscriptions($roles): array;

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   *   Optionally return the submit handler definition.
   */
  public function buildForm(&$form, FormStateInterface $form_state);

  /**
   * @param \Drupal\sane\Entity\EventInterface $event
   *
   * @return \Drupal\Core\Url
   */
  public function getRedirectUrl(EventInterface $event): Url;

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Creates all outstanding notifications for events of this plugin.
   */
  public function createNotifications(): void;

}
