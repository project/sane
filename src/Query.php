<?php

namespace Drupal\sane;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\sane\Entity\EventInterface;

class Query {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * @inheritDoc
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, Connection $database) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->database = $database;
  }

  /**
   * @param \Drupal\sane\Entity\EventInterface $event
   *
   * @return \Drupal\sane\Entity\NotificationInterface[]
   */
  public function findEventNotificationsForCurrentUser(EventInterface $event): array {
    try {
      return $this->entityTypeManager->getStorage('sane_notification')->loadByProperties([
        'event' => $event->id(),
        'uid' => $this->currentUser->id(),
      ]);
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
    return [];
  }

  /**
   * @param \Drupal\sane\PayloadInterface $payload
   *
   * @return \Drupal\sane\Entity\NotificationInterface[]
   */
  public function findNotificationsForCurrentUser($payload): array {
    $reference = $payload->getEventReference();
    $query = $this->database->select('sane_notification', 'n');
    $query->join('sane_event', 'e', 'n.event = e.id');
    /** @noinspection PhpUndefinedMethodInspection */
    $ids = $query
      ->fields('n', ['id'])
      ->condition('n.uid', $this->currentUser->id())
      ->condition('n.seen', 0)
      ->condition('e.reference', $reference)
      ->execute()
      ->fetchCol();
    if (!empty($ids)) {
      try {
        return $this->entityTypeManager->getStorage('sane_notification')
          ->loadMultiple($ids);
      }
      catch (InvalidPluginDefinitionException $e) {
        // TODO: Handle exception.
      }
      catch (PluginNotFoundException $e) {
        // TODO: Handle exception.
      }
    }
    return [];
  }

  /**
   * @param \Drupal\sane\Entity\EventInterface $event
   * @param int $uid
   *
   * @return \Drupal\sane\Entity\NotificationInterface[]
   */
  public function findSimilarEventNotifications(EventInterface $event, $uid): array {
    $query = $this->database->select('sane_notification', 'n');
    $query->join('sane_event', 'e', 'n.event = e.id');
    /** @noinspection PhpUndefinedMethodInspection */
    $ids = $query
      ->fields('n', ['id'])
      ->condition('n.uid', $uid)
      ->condition('n.delivered', 0)
      ->condition('n.seen', 0)
      ->condition('n.redundant', 0)
      ->condition('e.topic', $event->getTopic())
      ->condition('e.reference', $event->getPayload()->getEventReference())
      ->execute()
      ->fetchCol();
    if (!empty($ids)) {
      try {
        return $this->entityTypeManager->getStorage('sane_notification')
          ->loadMultiple($ids);
      }
      catch (InvalidPluginDefinitionException $e) {
        // TODO: Handle exception.
      }
      catch (PluginNotFoundException $e) {
        // TODO: Handle exception.
      }
    }
    return [];
  }

  /**
   * @return array
   */
  public function rolesAsSelectList(): array {
    static $roles;
    if (!isset($roles)) {
      $roles = [];
      try {
        foreach ($this->entityTypeManager->getStorage('user_role')
                   ->loadMultiple() as $role_name => $role) {
          if ($role->id() !== 'anonymous') {
            $roles[$role_name] = $role->label();
          }
        }
      }
      catch (InvalidPluginDefinitionException $e) {
        // TODO: handle exception.
      }
      catch (PluginNotFoundException $e) {
        // TODO: handle exception.
      }
    }
    return $roles;
  }

}
