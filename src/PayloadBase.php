<?php

namespace Drupal\sane;

/**
 * Base class for sane plugins.
 */
abstract class PayloadBase implements PayloadInterface {

  /**
   * @param array $payload
   *
   * @return PayloadInterface
   */
  final public static function fromArray(array $payload): PayloadInterface {
    $class = $payload['class'];
    unset($payload['class']);
    return $class::createFromArray($payload);
  }

  /**
   * @return array
   */
  final public function toArray(): array {
    $payload = $this->prepareArray();
    $payload['class'] = get_class($this);
    return $payload;
  }

}
