<?php

namespace Drupal\sane;

/**
 * Interface for sane recipient selection plugins.
 */
interface RecipientSelectionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * @param \Drupal\sane\PayloadInterface $payload
   *
   * @return int[]
   */
  public function getRecipients(PayloadInterface $payload): array;

}
