<?php

namespace Drupal\sane\Commands;

use Drupal\sane\Service;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class SaneCommands extends DrushCommands {

  /**
   * @var \Drupal\sane\Service
   */
  protected $sane;

  /**
   * SaneCommands constructor.
   *
   * @param \Drupal\sane\Service $sane
   */
  public function __construct(Service $sane) {
    parent::__construct();
    $this->sane = $sane;
  }

  /**
   * Create notifications.
   *
   * @usage sane:notifications:create
   *   Creates all outstanding notifications.
   *
   * @command sane:notifications:create
   * @aliases snc
   */
  public function createNotifications(): void {
    $this->sane->createNotifications();
  }

}
